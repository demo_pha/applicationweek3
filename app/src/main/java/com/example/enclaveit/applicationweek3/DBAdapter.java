package com.example.enclaveit.applicationweek3;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by enclaveit on 29/12/2016.
 */

public class DBAdapter extends SQLiteOpenHelper {
    private static final String DB_NAME = "applicationweek3";
    private static final String TABLE_NAME = "FAVORITE_CAFE";
    private static final int DB_VERSION = 1;
    private static final String KEY_NAME = "name";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LONG = "longitude";
    private static final String KEY_ADDRESS = "address";

    private static final String CREATE_TABLE = "create table "+TABLE_NAME+" ("
            +KEY_NAME+" text not null, "
            +KEY_ADDRESS + " text not null, "
            +KEY_LAT + " double not null, "
            +KEY_LONG + " double not null)";
    private static final String INIT_VALUES = "insert into "+TABLE_NAME+" values(\"Karty Coffee\",\"98 Bạch Đằng, Hải Châu 1, Q. Hải Châu Đà Nẵng, Việt Nam\",16.069200, 108.225094),"
            +"(\"Highlands Coffee Indochina Danang\",\"Indochina Riverside Tower, Tầng 1, 74 Bạch Đằng, Hải Châu 1, Hải Châu, Da Nang, Việt Nam\", 16.069876, 108.224953),"
            +"(\"Quán cà phê Phố Xưa 1\",\"17 Phan Đình Phùng, Hải Châu 1, Hải Châu, Đà Nẵng, Việt Nam\",16.069979, 108.223470)";

    public DBAdapter(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("Message","Create "+TABLE_NAME+" table");
        db.execSQL(CREATE_TABLE);
        db.execSQL(INIT_VALUES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("Drop table if exists "+TABLE_NAME);
        onCreate(db);
    }

    public ArrayList<CafeClass> getListCafes(){
        ArrayList<CafeClass> listCafes = new ArrayList<>();
        String sql = "select * from "+TABLE_NAME+" order by name asc";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor != null && cursor.moveToFirst()){
            do {
                CafeClass cafe = new CafeClass(cursor.getString(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3));
                listCafes.add(cafe);
            }while (cursor.moveToNext());
        }
        return listCafes;
    }

    public ArrayList<String> getListCafeNames(){
        ArrayList<String> listCafes = new ArrayList<>();
        String sql = "select * from "+TABLE_NAME+" order by name asc";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor != null && cursor.moveToFirst()){
            do {
                listCafes.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        return listCafes;
    }

    public CafeClass getCafe(String name){
        CafeClass cafe = new CafeClass();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+TABLE_NAME+" where name = \""+name+"\"", null);
        if(cursor != null && cursor.moveToFirst()){
            cafe.setName(name);
            cafe.setAddress(cursor.getString(1));
            cafe.setLatitude(cursor.getDouble(2));
            cafe.setLongitude(cursor.getDouble(3));
            //Log.i("Message", "Change to "+cursor.getDouble(cursor.getColumnIndexOrThrow("latitude"))+" , "+cursor.getDouble(cursor.getColumnIndexOrThrow("longitude")));
        }
        return cafe;
    }
}
