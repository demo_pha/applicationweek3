package com.example.enclaveit.applicationweek3.Thread;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.enclaveit.applicationweek3.R;
import com.example.enclaveit.applicationweek3.Toaster;

import org.w3c.dom.Text;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by enclaveit on 26/12/2016.
 */

public class ThreadActivity extends Activity implements View.OnClickListener {
    Button btnStartThread, btnSingleThread, btnThreadPool;
    TextView txtThread;
    ImageView imgStartThread;
    ProgressDialog progress;
    /*Get the number of available cores
    * (not always the same as the maximum number of cores)
    */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    /*Sets the amount of time an idle thread waits before terminating*/
    private static final int KEEP_ALIVE_TIME = 1000;
    /*Set the Time Unit to millisecond*/
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.MILLISECONDS;
    /*Used to update UI progress*/
    private int count = 0;

    /*This is the runnable task that we will run 100 times*/
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try{
                Thread.sleep(500);
            } catch (InterruptedException ex){
                ex.printStackTrace();
            }

            /*Update UI with progress*/
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    count++;
                    String msg = (count < 100)?"working...":"done";
                    updateStatus(msg+" "+count);
                }
            });
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thread_activity_layout);

        txtThread = (TextView) findViewById(R.id.txtThread);

        btnStartThread = (Button) findViewById(R.id.btnStartThread);
            btnStartThread.setOnClickListener(this);

        btnSingleThread = (Button) findViewById(R.id.btnSingleThread);
            btnSingleThread.setOnClickListener(this);

        btnThreadPool = (Button) findViewById(R.id.btnThreadPool);
            btnThreadPool.setOnClickListener(this);

        imgStartThread = (ImageView) findViewById(R.id.imgStartThread);
        progress = new ProgressDialog(ThreadActivity.this);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setMessage("Downloading image");
        Log.i("Message", "Thread Activity -- "+progress.getProgress());
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnStartThread:
                startThread();
                break;
            case R.id.btnSingleThread:
                singleThread();
                break;
            case R.id.btnThreadPool:
                threadPool();
                break;
        }
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String result = bundle.getString("message");
            Toaster.makeToast(getApplicationContext(), result, 1500);
            progress.dismiss();
            imgStartThread.setImageResource(R.drawable.anime_girl_wallpaper);

        }
    };

    public void startThread(){
        progress.show();
        /*Create new Thread*/
        Thread startThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    Message message = handler.obtainMessage();
                    Bundle bundle = new Bundle();
                    String msg = "Download Image successful";
                    bundle.putString("message", msg);
                    message.setData(bundle);
                    handler.sendMessage(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        startThread.start();
        Log.i("Message", "Start Thread");
    }

    public void singleThread(){
        count = 0;
        Executor singleThreadExecutor = Executors.newSingleThreadExecutor();
        for (int i=0; i<100 ; i++)
            singleThreadExecutor.execute(runnable);
    }

    public void threadPool(){
        count = 0 ;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                                                        NUMBER_OF_CORES+5,  // Initial Pool size
                                                         NUMBER_OF_CORES+8, // Max Pool size
                                                        KEEP_ALIVE_TIME,    // Time idle wait before terminating
                                                        KEEP_ALIVE_TIME_UNIT,   // Sets the Time Unit for KEEP_ALIVE_TIME
                                                        new LinkedBlockingDeque<Runnable>());   // Work Queue
        Log.i("Message", "Thread Pool number of cores = "+NUMBER_OF_CORES);
        Log.i("Message", "Thread Pool active count = "+threadPoolExecutor.getActiveCount());
        Log.i("Message", "Thread Pool task count = "+threadPoolExecutor.getTaskCount());
        for (int i=0; i<100 ; i++) {
            threadPoolExecutor.execute(runnable);
            Log.i("Message", "Thread Pool active count = " + threadPoolExecutor.getActiveCount());
            Log.i("Message", "Thread Pool task count = " + threadPoolExecutor.getTaskCount());
        }
        Log.i("Message", "Thread Pool active count = " + threadPoolExecutor.getActiveCount());
        Log.i("Message", "Thread Pool task count = " + threadPoolExecutor.getTaskCount());
    }

    public void updateStatus(String s){
        txtThread.setText(s);
    }

}
