package com.example.enclaveit.applicationweek3.AsyncTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.enclaveit.applicationweek3.WebService.RESTFulWebServiceActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by enclaveit on 27/12/2016.
 */

public class AsyncTaskGetServerData extends AsyncTask<String, String, String> {
    /*Required initialization*/
    private HttpClient client = new DefaultHttpClient();
    private HttpGet httpGet ;
    private HttpResponse response;
    private HttpEntity httpEntity;

    String content = "";
    String dataToJSON = "";
    private String error = null;
    private ProgressDialog progressDialog;
    private int sizeData = 0 ;
    private Context context;

    public AsyncTaskGetServerData(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        Log.i("Message", "On PreExecute Get Data Server");
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait while get data from server !");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i("Message", "Do In Background Get Data Server");
        Log.i("Message", "URL = "+params[0]);
        httpGet = new HttpGet(params[0]);
        try {
            response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if(statusCode == 200){
                Log.i("Message", "Get Successful");
                httpEntity = response.getEntity();
                InputStream inputStream = httpEntity.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null){
                    stringBuilder.append(line);
                }
                content = stringBuilder.toString();
                Log.i("Message", "DATA : "+content);
            }else {
                Log.i("Message", "Fail to load data from Server");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content; /*Execute onPostExecute next*/
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i("Message", "On Post Execute Get Data Server");
        progressDialog.dismiss();
        /*Convert content to JSON*/
        try {
            JSONArray jsonArray = new JSONArray(result);
            Log.i("Message","JSON Length = "+jsonArray.length());
            /*Get each JSONObject*/
            for (int i=0; i< jsonArray.length(); i++) {
                JSONObject jObject = jsonArray.getJSONObject(i);
                dataToJSON += jObject.getString("appeId") + " - "+jObject.getString("survId")+ " - "
                                + jObject.getString("location")+" - "+ jObject.getString("surveyDate")
                                + jObject.getString("surveyTime") + " - " + jObject.getString("inputUserId") + " - "
                                + jObject.getString("inputTime") + "\n";
            }
            Log.i("Message", "DATA TO JSON : "+dataToJSON);
            Intent sendResult  = new Intent("com.example.applicationweek3.CUSTOM_INTENT");
                    sendResult.putExtra("result", "complete");
                    context.sendBroadcast(sendResult);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Log.i("Message", "On Progress UpdateGet Data Server");

    }

    public String getContent(){
        return  content;
    }
     public String getDataToJSON(){
         return dataToJSON;
     }

}
