package com.example.enclaveit.applicationweek3;

import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by enclaveit on 29/12/2016.
 */

public class LocationClass {
    LatLng myLocation;

    public LocationClass(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Service.LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            // Getting latitude of the current location
            double latitude = location.getLatitude();

            // Getting longitude of the current location
            double longitude = location.getLongitude();

            // Creating a LatLng object for the current location

            myLocation = new LatLng(latitude, longitude);
            Log.i("Message", "My Location is : "+latitude+" , "+longitude);

        }
        // Add Default Position
        if(myLocation == null) myLocation = new LatLng(16.053324, 108.217704);
    }

    public LatLng getMyLocation(){
        return myLocation;
    }
}
