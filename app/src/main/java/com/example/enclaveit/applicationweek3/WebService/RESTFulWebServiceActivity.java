package com.example.enclaveit.applicationweek3.WebService;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.enclaveit.applicationweek3.AsyncTask.AsyncTaskGetServerData;
import com.example.enclaveit.applicationweek3.R;
import com.example.enclaveit.applicationweek3.Toaster;

/**
 * Created by enclaveit on 27/12/2016.
 */

public class RESTFulWebServiceActivity extends Activity {
    private Button btnGetServerData;
    private TextView txtServerData, txtConvertToJSON;
    AsyncTaskGetServerData asyncTaskGetServerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restful_web_service_activity_layout);
        Log.i("Message", "On Create RESTFul WebService");
        txtServerData = (TextView) findViewById(R.id.txtServerResult);
        txtConvertToJSON = (TextView) findViewById(R.id.txtConvertToJSON);
        btnGetServerData = (Button) findViewById(R.id.btnGetServerData);
            btnGetServerData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /* WebServer Request URL*/
                    String URL = "http://extjs.org.cn/extjs/examples/grid/survey.html";
                    asyncTaskGetServerData = new AsyncTaskGetServerData(RESTFulWebServiceActivity.this);
                    asyncTaskGetServerData.execute(URL);
                }
            });
        /*Waiting for complete get server data*/
        IntentFilter intentFilter = new IntentFilter("com.example.applicationweek3.CUSTOM_INTENT");
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("Message", "On Receiver Web Service Activity");
                String result = intent.getStringExtra("result");
                if(result.equalsIgnoreCase("complete")){
                    Log.i("Message", "Received success");
                    txtServerData.setText(asyncTaskGetServerData.getContent());
                    txtConvertToJSON.setText(asyncTaskGetServerData.getDataToJSON());
                    Toaster.makeToast(getApplicationContext(), "Get Server Data Successfully", 2000);
                }
            }
        };
                    /*Register our receiver*/
        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Message", "On Start RESTFul WebService");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Message", "On Resume RESTFul WebService");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Message", "On Pause RESTFul WebService");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Message", "On Stop RESTFul WebService");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
