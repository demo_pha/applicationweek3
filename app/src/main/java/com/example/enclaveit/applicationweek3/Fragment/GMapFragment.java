package com.example.enclaveit.applicationweek3.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.example.enclaveit.applicationweek3.CafeClass;
import com.example.enclaveit.applicationweek3.DBAdapter;
import com.example.enclaveit.applicationweek3.LocationClass;
import com.example.enclaveit.applicationweek3.R;
import com.example.enclaveit.applicationweek3.Toaster;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Parcelable;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enclaveit on 29/12/2016.
 */

public class GMapFragment extends AppCompatActivity implements OnMapReadyCallback {

    CoordinatorLayout coordinatorLayout;
    GoogleMap gMap;
    LatLng myPosition;
    Spinner chooseCafe ;
    DBAdapter db;
    boolean isClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gmap_fragment_layout);
        isClicked = false;
        Log.i("Message", "On GMap Fragment Create");
        /*Using ToolBar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Favorite Cafe");
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        if(gMap == null) {
            MapFragment mMapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment));
            mMapFragment.getMapAsync(this);
            Log.i("Message", "ReCreate GMap");
        }

        chooseCafe = (Spinner) findViewById(R.id.chooseCafe);
        db= new DBAdapter(this);
        ArrayList<String> listCafeNames = db.getListCafeNames();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listCafeNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chooseCafe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (isClicked){
                    Toaster.makeToast(getApplication(),"Cafe Name is "+adapterView.getItemAtPosition(position).toString(),2000);
                    CafeClass cafe = db.getCafe(adapterView.getItemAtPosition(position).toString());
                    Log.i("Message", "Change to "+cafe.getLatitude()+" , "+cafe.getLongitude());
                    LatLng cafeLatLng = new LatLng(cafe.getLatitude(), cafe.getLongitude());
                    if(cafeLatLng != null) {
                        if(gMap != null) {
                            gMap.clear();
                            Log.i("Message", "Clear all old Position");
                        }else Log.i("Message", "GMap is null");
                        gMap.addMarker(new MarkerOptions().position(cafeLatLng).title(cafe.getName()));
                        gMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        gMap.setBuildingsEnabled(true);
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cafeLatLng, 17));
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(cafeLatLng)
                                .zoom(18)
                                .tilt(30)   // Sets the tilt of the camera to 30 degree
                                .bearing(0)    // Sets the orientation of the camera to north
                                .build();
                        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000, null); //set duration
                    }
                }
                isClicked = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        chooseCafe.setAdapter(adapter);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("Message", "On Map Ready");
        gMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
            ActivityCompat.requestPermissions( this, new String[] {android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    2 );
        }
        gMap.setMyLocationEnabled(true);
        LocationClass myLocationClass = new LocationClass(this);
            myPosition = myLocationClass.getMyLocation();
        if(myPosition != null) {
            if(gMap != null) gMap.clear();  //Remove all old position
            gMap.addMarker(new MarkerOptions().position(myPosition).title("My Location"));
            gMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            gMap.setBuildingsEnabled(true);
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 18));
        }else {
            new AlertDialog.Builder(this)
                .setTitle("Setting Location")
                .setMessage("This app required Location Permission.")
                .setPositiveButton("Go to setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
        }
    }

    /*Using ToolBar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*Inflate Menu; This adds items to menu if it is presented*/
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
     /*Event when click items of Menu ToolBar*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.normalLayout:
                Toaster.makeToast(this, "You clicked the Normal Map Type", 1500);
                gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybridLayout:
                Toaster.makeToast(this, "You clicked the Hybrid Map Type", 1500);
                gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.satelliteLayout:
                Toaster.makeToast(this, "You clicked the Satellite Map Type", 1500);
                gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.terrainLayout:
                Toaster.makeToast(this, "You clicked the Terrain Map Type", 1500);
                gMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Message", "On GMap Fragment Start()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("Message", "On GMap Fragment Saved Instance State()");
        outState.putString("gmap", "Saved GMap Instance");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("Message", "On GMap Fragment Restore Instance State()");
        Log.i("Message", "Message : "+savedInstanceState.getString("gmap"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Message", "On GMap Fragment Pause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Message", "On GMap Fragment Resume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Message", "On GMap Fragment ReStart()");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Message", "On GMap Fragment Stop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Message", "On GMap Fragment Destroy()");
    }

}
