package com.example.enclaveit.applicationweek3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.enclaveit.applicationweek3.AsyncTask.AsyncTaskActivity;
import com.example.enclaveit.applicationweek3.AsyncTask.AsyncTaskGetServerData;
import com.example.enclaveit.applicationweek3.Fragment.GMapFragment;
import com.example.enclaveit.applicationweek3.Thread.ThreadActivity;
import com.example.enclaveit.applicationweek3.WebService.RESTFulWebServiceActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnThread, btnAsyncTask, btnGetServerData, btnLocation, btnGoogleMap;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnThread = (Button) findViewById(R.id.btnThread);
            btnThread.setOnClickListener(this);

        btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
            btnAsyncTask.setOnClickListener(this);

        btnGetServerData = (Button) findViewById(R.id.btnGetServerData);
            btnGetServerData.setOnClickListener(this);
        btnLocation = (Button) findViewById(R.id.btnLocation);
            btnLocation.setOnClickListener(this);
        btnGoogleMap = (Button) findViewById(R.id.btnGoogleMap);
            btnGoogleMap.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnThread:
                intent = new Intent(this, ThreadActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAsyncTask:
                intent = new Intent(this, AsyncTaskActivity.class);
                startActivity(intent);
                break;
            case R.id.btnGetServerData:
                intent = new Intent(this, RESTFulWebServiceActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLocation:
                intent = new Intent(this, LocationActivity.class);
                startActivity(intent);
                break;
            case R.id.btnGoogleMap:
                intent = new Intent(this, GMapFragment.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Message", "Main On Start");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Message", "Main On Resume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Message", "Main On Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Message", "Main On Destroy");
    }

}
