package com.example.enclaveit.applicationweek3;

import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by PHA on 27/12/2016.
 */

/*https://www.youtube.com/watch?v=h7LUNCC0U1U*/
public class LocationActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    Button btnGetLastLocation;
    TextView txtLatitude, txtLongitude;
    GoogleApiClient mGoogleApiClient;
    Location mLocation;
    LocationRequest locationRequest;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 1;
    private final static int MY_PERMISSION_ACCESS_FINE_LOCATION = 2;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_activity_layout);
        Log.i("Message", "This device is supported ? "+checkPlayServices());
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            Log.i("Message", "Create Google API Client");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        txtLatitude = (TextView) findViewById(R.id.txtLatitude);
        txtLongitude = (TextView) findViewById(R.id.txtLongitude);
        btnGetLastLocation = (Button) findViewById(R.id.btnGetLastLocation);
        btnGetLastLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestLocationUpdates();
                getLastLocation();
            }
        });

    }

    @Override
    protected void onStart() {
        Log.i("Message", "Location Activity On Start");
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Message", "Location Activity On Resume");
        if(mGoogleApiClient != null)    mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("Message", "On Connected Using Location");
        requestLocationUpdates();

    }

    public void requestLocationUpdates(){
        if (mLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_ACCESS_FINE_LOCATION);
                ActivityCompat.requestPermissions( this, new String[] {android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSION_ACCESS_COURSE_LOCATION );
            }
            Log.i("Message", "Google API Client is Connected ? "+mGoogleApiClient.isConnected());

            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLocation == null)  {
                Log.i("Message", "My Location is null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Log.i("Message", "Location changed to "+String.valueOf(location.getLatitude()) + " , "+String.valueOf(location.getLongitude()));
                        mLocation = location;
                    }
                });
            }else {
                //Toaster.makeToast(this, "Latitude = "+String.valueOf(mLocation.getLatitude())+" --- Longitude = "+String.valueOf(mLocation.getLongitude()), 2000);
                getLastLocation();
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("Message","Connect Failed with error : "+connectionResult.getErrorMessage());
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Message", "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }

    public void getLastLocation(){
        if(mLocation != null){
            txtLatitude.setText("Latitude : "+String.valueOf(mLocation.getLatitude()));
            txtLongitude.setText("Longitude : "+String.valueOf(mLocation.getLongitude()));
            Toaster.makeToast(this, "Latitude = "+String.valueOf(mLocation.getLatitude())+" --- Longitude = "+String.valueOf(mLocation.getLongitude()), 2000);
        }
    }
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

}
