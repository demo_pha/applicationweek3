package com.example.enclaveit.applicationweek3.AsyncTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.enclaveit.applicationweek3.R;


/**
 * Created by enclaveit on 27/12/2016.
 */

public class AsyncTaskActivity extends Activity {
    private Button btnAsyncTaskRunner;
    private TextView txtResult;
    private EditText txtInTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.async_task_activity_layout);

        txtResult = (TextView) findViewById(R.id.txtViewAsyncTaskResult);
        txtInTime  = (EditText) findViewById(R.id.txtInTime);
        btnAsyncTaskRunner = (Button) findViewById(R.id.btnAsyncTaskRunner);
            btnAsyncTaskRunner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(txtInTime.getText().toString().equals("") || Integer.parseInt(txtInTime.getText().toString()) <=0){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AsyncTaskActivity.this);
                            alertDialog.setMessage("The timing must be greater than 0 and required");
                            alertDialog.setIcon(R.mipmap.warning);
                            alertDialog.show();
                    }
                    else {
                        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
                        String sleepTime = txtInTime.getText().toString();
                        asyncTaskRunner.execute(sleepTime);
                    }

                }
            });

        Log.i("Message", "AsyncTaskActivity On Create");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Message", "AsyncTaskActivity On Start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Message", "AsyncTaskActivity On Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Message", "AsyncTaskActivity On Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Message", "AsyncTaskActivity On Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Message", "AsyncTaskActivity On Destroy");
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String>{
        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            Log.i("Message"," On PreExecute AsyncTaskActivity");
            progressDialog = new ProgressDialog(AsyncTaskActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle("Async Task Runner");
            progressDialog.setMessage("Waiting for "+txtInTime.getText().toString()+" seconds");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i("Message"," Do In Background AsyncTaskActivity");
            publishProgress("Sleeping....."); /*Calls onProgressUpdate() with params*/
            try {
                int time = Integer.parseInt(params[0])*1000;
                Thread.sleep(time);
                resp = "Slept for " + params[0] + " seconds";
            } catch (InterruptedException e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp; /*Execute onPostExecute next with String param*/
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.i("Message"," On Progress Update AsyncTaskActivity");
            txtResult.setText(values[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("Message"," On Post Execute AsyncTaskActivity");
            Log.i("Message", "Result : "+result);
            progressDialog.dismiss();
            txtResult.setText(result);
        }

    }
}
